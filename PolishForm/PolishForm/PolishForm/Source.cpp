#include <iostream>

using namespace std;

//stack part

struct node
{
	node *next;
	int data;
};

struct stack
{
	node* stack_pointer;
};

void stack_init(stack& stiv)
{
	stiv.stack_pointer = nullptr;
}

char stack_top(const stack stiv)
{
	if (nullptr != stiv.stack_pointer)
	{
		return stiv.stack_pointer->data;
	}
}

char stack_pop(stack& stiv)
{
	if (nullptr != stiv.stack_pointer)
	{
		node* p = stiv.stack_pointer;
		char aux = stack_top(stiv);
		stiv.stack_pointer = stiv.stack_pointer->next;
		delete p;
		return aux;
	}
}

void stack_push(stack& stiv, char value)
{
	node* p = new node;
	p->data = value;
	p->next = stiv.stack_pointer;
	stiv.stack_pointer = p;
}

bool stack_is_empty(const stack stiv)
{
	return (nullptr == stiv.stack_pointer);
}


//int main()
//{
//	stack stiva;
//	stack_init(stiva);
//	
//	/*
//	 * begin polish form conversion
//	 */
//
//	char sir1[100]="120+(2*3+4)/(5-6)";
//	char sir2[100];
//	int i, k=0;
//	for (i=0; i<strlen(sir1); ++i)
//	{
//		if (sir1[i]=='(')
//		{
//			stack_push(stiva, sir1[i]);
//		}
//		if (sir1[i]>='0' && sir1[i]<='9' || sir1[i]==' ')
//		{
//			sir2[k++]=sir1[i];
//		}
//		if (sir1[i]==')')
//		{
//			while (stack_top(stiva)!='(' && !stack_is_empty(stiva))
//			{
//				sir2[k++]=stack_pop(stiva);
//			}
//			stack_pop(stiva);
//			//scot '('
//		}
//		if (sir1[i]=='+' || sir1[i]=='-')
//		{
//			while (stack_top(stiva) !='(' && !stack_is_empty(stiva))
//			{
//				sir2[k++]=stack_pop(stiva);
//			}
//			stack_push(stiva, sir1[i]);
//		}
//		if (sir1[i]=='*' || sir1[i]=='/')
//		{
//			while (stack_top(stiva)=='*' || stack_top(stiva)=='/')
//			{
//				sir2[k++]=stack_top(stiva);
//			}
//			stack_push(stiva, sir1[i]);
//		}
//	}
//	while (!stack_is_empty(stiva))
//	{
//		sir2[k++]=stack_pop(stiva);
//	}
//	sir2[k]='\0';
//	cout<<sir2;
//}

int main(void)
{
	stack stiva;
	stack_init(stiva);
	char sir1[] ="356*+";
	for (int i = 0; i < strlen(sir1); ++i)
	{
		if (isdigit(sir1[i]))
		{
			stack_push(stiva, sir1[i] - '0');
		}
		else
		{
			int t1 = stack_pop(stiva);
			int t2 = stack_pop(stiva);
			char ch = sir1[i];
			switch (ch)
			{
			case '+':
				t1+=t2;
				stack_push(stiva, t1);
				break;
			case '*':
				t1*=t2;
				stack_push(stiva, t1);
				break;
			}
		}
	}
	if(stack_is_empty(stiva))
	{
		cout<<"lost";
	}
	int aux=stack_pop(stiva);
	if (!stack_is_empty(stiva))
	{
		cout<<"ost again;";
	}
	cout<<aux;
	return 0;
}