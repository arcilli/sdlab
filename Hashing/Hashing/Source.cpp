#include <iostream>
#include "Hashing.h"

using namespace std;

int main(void)
{
	NODE_t* HT[M];

	//dont forget to initialize the HashTable
	Hash_init(HT);
	int n;
	cout << "How any elements? ";
	cin >> n;
	for (int i = 0; i < n; ++i)
	{
		Hash_insertion(HT);
	}
	Hash_print(HT);
	Hash_deleteWithName(HT);
	cout << "After deleting: ";
	Hash_print(HT);
	return 0;
}