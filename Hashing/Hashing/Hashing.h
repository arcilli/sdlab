#pragma once
#include <cstdint>
#define M 13

struct Student
{
	char name[30];
	int age;
};

struct NODE_t
{
	NODE_t* next;
	Student person;
};

void Hash_init(NODE_t* HashTable[]);

//dispersionFunction
int Hash_function(char* string);
void Hash_insertion(NODE_t* HashTable[]);
NODE_t* Hash_search(NODE_t* HashTable[], char* string);
void Hash_print(NODE_t* HashTable[]);
void Hash_deleteWithName(NODE_t* HashTable[]);

void Student_read(Student&);