#include<iostream>
#include "Hashing.h"

using namespace std;

//initializate the HashTable
void Hash_init(NODE_t* HashTable[])
{
	for (int i = 0; i < M; ++i)
	{
		HashTable[i] = nullptr;
	}
}

int Hash_function(char* string)
{
	int s = 0;
	for (int i = 0; i < strlen(string); ++i)
	{
		s += string[i];
	}
	return s%M;
}

void Hash_insertion(NODE_t* HashTable[])
{
	Student std;
	Student_read(std);
	NODE_t* node=new NODE_t;
	node->person = std;
	int aux = Hash_function(std.name);
	if (nullptr == HashTable[aux]) //first data with this hashCode
	{
		HashTable[aux] = node;
		node->next = nullptr;
	}
	else
	{
		//trebuie verificat daca nu exista deja nodul/ studentul pe care vreau sa-l inserez
		if (nullptr == Hash_search(HashTable, std.name))
		{
			node->next = HashTable[aux];
			HashTable[aux] = node;
		}
		else
		{
			cout << "It's already in the database. ";
		}
	}
}

NODE_t* Hash_search(NODE_t* HashTable[], char* string)
{
	int aux = Hash_function(string);
	NODE_t* p = HashTable[aux];
	while (nullptr != p)
	{
		if (strcmp(p->person.name, string) == 0)
		{
			return p;
		}
		p = p->next;
	}
	return nullptr;
}

void Hash_print(NODE_t* HashTable[])
{
	for (int i = 0; i < M; ++i)
	{
		if (nullptr != HashTable[i])
		{
			cout << "Records with H key: " << i<<endl;
			NODE_t* p = HashTable[i];
			while (nullptr != p)
			{
				cout << p->person.name << "  " << p->person.age << " ani";
				cout << endl;
				p = p->next;
			}
			cout << endl << endl;
		}
	}
}

void Hash_deleteWithName(NODE_t* HashTable[])
{
	char aux[100];
	cout << "Name to delete: ";
	cin >> aux;
	int h = Hash_function(aux);
	NODE_t *p = HashTable[h];
	if (strcmp(p->person.name, aux) == 0)
	{
		HashTable[h] = p->next;
		delete p;
		p = nullptr;
	}
	else
	{
		while (nullptr != p)
		{
			if (strcmp(p->next->person.name, aux) == 0)
			{
				NODE_t *q = p->next;
				p->next = p->next->next;
				delete q;
				q = nullptr;
			}
			p = p->next;
		}
	}
}

void Student_read(Student& aux)
{
	cout << "name?";
	cin >> aux.name;
	cout << "age?";
	cin >> aux.age;
}