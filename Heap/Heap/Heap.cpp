#include <iostream>
#include "Heap.h"

using namespace std;

void heap_insert(atom_t array[], int &n, atom_t value)
{
	n = n + 1;
	//n is current number of elements
	int child = n, father = n / 2;
	array[n] = value;
	while (child > 1)
	{
		if (array[child] > array[father])
		{
			swap(array[father], array[child]);
			child = father;
			father = father / 2;
		}
		else
		{
			child = 1;
		}
	}
}

int heap_delete(atom_t array[], int&n)
{
	atom_t aux = array[1];
	array[1] = array[n];
	n--;
	int father = 1;
	int child = 2;
	while (child <= n)
	{
		if (array[child] < array[child + 1] && child + 1 <= n)
		{
			//aleg cel mai mare copil
			child++;
		}
		if (array[father] < array[child])
		{
			swap(array[father], array[child]);
			father = child;
			child = child * 2;
		}
		else
		{
			child = n + 1;
		}
	}
	return aux;
}

//metoda de creare arbore in-place, Up-Down
void heap_heapfy(atom_t array[], const int n)
{
	int N = 1;
	//presupun ca am la inceput un vector care este deja heap, in care deja am 
	//N elemente care formeaza heap-ul
	for (int i = 2; i < n; ++i)
	{
		heap_insert(array, N, array[i]);
	}
}

//metoda de creare arbore, down-top
void heap_heapfy2(atom_t array[], int n)
{
	for (int i = n / 2; i >= 1; --i)
	{
		percolate(array, n, i);
	}
}

void percolate(atom_t array[], int &n, int i)
{
	int father = i, child = 2 * i;
	while (child <= n)
	{
		if (array[child] < array[child + 1] && child + 1 <= n)
		{
			child++;
		}
		if (array[father] < array[child])
		{
			swap(array[father], array[child]);
			father = child;
			child = child * 2;
		}
		else
			child = n + 1;
	}
}



/************************************MINHEAP*******************************/

void min_heap_insert(atom_t array[], int &n, atom_t value)
{
	n++;
	array[n] = value;
	int child = n;
	int father = child / 2;
	while (child > 1)
	{
		if (array[child] < array[father])
		{
			swap(array[child], array[father]);
			child = father;
			father = father / 2;
		}
		else
		{
			child = 1;
		}
	}
}

atom_t min_heap_delete(atom_t array[], int&n)
{
	int aux = array[1];
	//aux trebuie returnat la sfarsit
	array[1] = array[n];
	n--;
	int father = 1;
	int child = 2 * father;
	while (child <= n)
	{
		if (array[child] > array[child + 1] && child + 1 <= n)
		{
			child++;
		}
		if (array[father] > array[child])
		{
			swap(array[father], array[child]);
			father = child;
			child = 2 * father;
		}
		else
		{
			child = n + 1;
		}
	}
	return aux;
}

void heap_sort(atom_t array[], int n)
{
	while (n>=1) 
	{
		atom_t aux=min_heap_delete(array, n);
		array[n+1]=aux; 
	}
}
