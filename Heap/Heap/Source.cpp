#include <iostream>
#include <ctime>
#include "Heap.h"

using namespace std;

void swap(atom_t *a, atom_t *b)
{
	atom_t temp = *a;
	*a = *b;
	*b = temp;
}

// A function to generate a random permutation of arr[]
void randomize(atom_t arr[], int n)
{
	// Use a different seed value so that we don't get same
	// result each time we run this program
	srand(time(nullptr));

	// Start from the last element and swap one by o ne. We don't
	// need to run for the first element that's why i > 0
	for (int i = n - 1; i >1; i--)
	{
		// Pick a random index from 0 to i
		int j = rand() % (i + 1)+1;

		// Swap arr[i] with the element at random index
		swap(&arr[i], &arr[j]);
	}
}

int main()
{
	/* MAX HEAP*/
	/*atom_t array[DIMMAX];
	atom_t arraycpy[DIMMAX];
	int n=0;
	int x=1;
	int i=1;
	while (x)
	{
		cout<<"value: ";
		cin>>x;
		heap_insert(array,n,x);
		arraycpy[i++]=x;
	}
	cout<<endl<<"after creating heap: ";
	for (int i=1; i<=n; ++i)
	{
		cout<<array[i]<<"  ";
	}
	cout<<endl<<"Intiail vector:";
	for (int j=1; j<i; ++j)
	{
		cout<<arraycpy[j]<<"  ";
	}

	cout<<endl<<"deleting the head:";
	heap_delete(array, n);
	for (int i=1; i<=n; ++i)
	{
		cout<<array[i]<<"  ";
	}

	cout<<endl<<"Lets randomize the vector:";
	randomize(array, n);
	for (int i=1; i<=n; ++i)
	{
		cout<<array[i]<<"  ";
	}
	heap_heapfy(array, n);
	cout<<endl<<"after heapfy: ";

	for (int i=1; i<=n; ++i)
	{
		cout<<array[i]<<"  ";
	}
	cout<<endl<<"Lets randomize the vector:";
	randomize(array, n);
	for (int i=1; i<=n; ++i)
	{
		cout<<array[i]<<"  ";
	}
	heap_heapfy2(array, n);
	cout<<endl<<"after heapfy with second function: ";

	for (int i=1; i<=n; ++i)
	{
		cout<<array[i]<<"  ";
	}
	cout<<endl<<"sorting: ";
	heap_sort(array, n);
	for (int i=1; i<=n; ++i)
	{
		cout<<array[i]<<"  ";
	}*/
	/*
	 *
	 */

	/*MINHEAP*/
	atom_t array2[DIMMAX];
	atom_t arraycpy2[DIMMAX];
	int n=0;
	int x=1;
	int i=1;
	while (x)
	{
		cout<<"value: ";
		cin>>x;
		min_heap_insert(array2,n,x);
		arraycpy2[i++]=x;
	}
	cout<<endl<<"after creating heap: ";
	for (int i=1; i<=n; ++i)
	{
		cout<<array2[i]<<"  ";
	}
	cout<<endl<<"Intiail vector:";
	for (int j=1; j<i; ++j)
	{
		cout<<arraycpy2[j]<<"  ";
	}

	//cout<<endl<<"deleting the head:";
	//min_heap_delete(array2, n);
	//for (int i=1; i<=n; ++i)
	//{
	//	cout<<array2[i]<<"  ";
	//}
	
	cout<<endl<<"sorting: ";
	heap_sort(array2, n);
	for (int i=1; i<=n; ++i)
	{
		cout<<array2[i]<<"  ";
	}
	return 0;
}