#pragma once
#define DIMMAX 100

typedef int atom_t;

void heap_insert(atom_t array[], int &n, atom_t value);
int heap_delete(atom_t array[], int&n);
void percolate(atom_t array[], int &n, int i);

//metoda de creare arbore in-place, Up-Down
void heap_heapfy(atom_t array[], int n);

//metoda de creare arbore, down-top
void heap_heapfy2(atom_t array[], int n);

/*
 *
 */
/***********************MINHEAP FUNCTIONS*******************/
void min_heap_insert(atom_t array[], int &n, atom_t value);
atom_t min_heap_delete(atom_t array[], int&n);

/*********************HEAP SORT************/
void heap_sort(atom_t array[],  int n);