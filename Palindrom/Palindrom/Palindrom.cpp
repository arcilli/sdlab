#include <iostream>
#include "Palindrom.h"

using namespace std;

void OrderedStack_init(OrderedStack& stiv)
{
	stiv.stackPointer = -1;
}

bool OrderedStack_isEmpty(OrderedStack stiv)
{
	return (-1 == stiv.stackPointer);
}

bool OrderedStack_isFull(OrderedStack stiv)
{
	return(DIMMAX == stiv.stackPointer);
}

bool OrderedStack_push(OrderedStack& stiv, ATOM value)
{
	if (OrderedStack_isFull(stiv))
	{
		return false;
	}
	else
	{
		stiv.stackPointer++;
		stiv.data[stiv.stackPointer] = value;
		return true;
	}
}

ATOM OrderedStack_pop(OrderedStack& stiv)
{
	if (OrderedStack_isEmpty(stiv))
	{
		cout << "Empty list";
		//exit(0);
	}
	else
	{
		stiv.stackPointer--;
		return stiv.data[stiv.stackPointer + 1];
	}
}

void OrderedQueue_init(OrderedQueue& lst)
{
	lst.head = lst.tail = 0;
}

bool OrderedQueue_isEmpty(OrderedQueue& lst)
{
	return(0 == lst.head);
}

bool OrderedQueue_isFull(OrderedQueue& lst)
{
	return (OrderedQueue_nextPosition(lst.tail) == lst.head);
}

bool OrderedQueue_put(OrderedQueue& lst, ATOM value)
{
	if (OrderedQueue_isFull(lst))
	{
		return false;
	}
	else
	{
		lst.data[lst.tail] = value;
		lst.tail = OrderedQueue_nextPosition(lst.tail);
		return true;
	}
}

ATOM OrderedQueue_get(OrderedQueue& lst)
{
	
	if (OrderedQueue_isEmpty(lst))
	{
		cout << "Empty list";
	}
	else
	{
		ATOM aux;
		aux = lst.data[lst.head];
		lst.head = OrderedQueue_nextPosition(lst.head);
		return aux;
	}
}

int OrderedQueue_nextPosition(int index)
{
	if (index < DIMMAX - 1)
	{
		index++;
	}
	else
	{
		index = 0;
	}
	return index;
}

void OrderedStack_print(const OrderedStack stck)
{
	for (int i = stck.stackPointer; i >= 0; --i)
	{
		cout << stck.data[i] << "  ";
	}
}

void OrderedQueue_print(OrderedQueue lst)
{
	while (lst.head != lst.tail) 
		//because lst.tail it's the next free position it's fine to go until head != tail:
		//the real tail is covered in last case
	{
		cout << lst.data[lst.head] << "  ";
		lst.head = OrderedQueue_nextPosition(lst.head);
	}
}

bool Palindrome_test(OrderedQueue lst, OrderedStack stck)
{
	int nrLst=0, nrStck=0;
	while (lst.head != lst.tail)
	{
		nrLst = nrLst * 10 + lst.data[lst.head];
		lst.head = OrderedQueue_nextPosition(lst.head);
	}
	for (int i = stck.stackPointer; i >= 0; --i)
	{
		nrStck = nrStck * 10 + lst.data[i];
	}
	cout << nrLst << "\t\t" << nrStck<<endl;
	return (nrLst == nrStck);
}