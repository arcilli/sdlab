#include <iostream>
#include "Palindrom.h"

using namespace std;

int main(void)
{
	OrderedStack stv;
	OrderedStack_init(stv);
	OrderedQueue lst;
	OrderedQueue_init(lst);
	int x;
	cout << "value: ";
	cin >> x;
	while (x)
	{
		OrderedQueue_put(lst, x%10);
		OrderedStack_push(stv, x%10);
		x = x / 10;
	}
	cout << endl;
	OrderedQueue_print(lst);
	cout << endl;
	OrderedStack_print(stv);
	cout << endl << endl;
	cout << Palindrome_test(lst, stv);
	return 0;
}