#pragma once

typedef int ATOM;
#define DIMMAX 100

struct OrderedQueue
{
	int head, tail;
	ATOM data[DIMMAX];
};

struct OrderedStack
{
	int stackPointer;
	ATOM data[DIMMAX];
};

//isEmpty, isFull, get/pop, push/put, init nextPosition @ OrderedQueue

void OrderedStack_init(OrderedStack&);
bool OrderedStack_isEmpty(OrderedStack);
bool OrderedStack_isFull(OrderedStack);
bool OrderedStack_push(OrderedStack&, ATOM);
ATOM OrderedStack_pop(OrderedStack&);
void OrderedStack_print(const OrderedStack);

void OrderedQueue_init(OrderedQueue&);
bool OrderedQueue_isEmpty(OrderedQueue&);
bool OrderedQueue_isFull(OrderedQueue&);
bool OrderedQueue_put(OrderedQueue&, ATOM);
ATOM OrderedQueue_get(OrderedQueue&);
int OrderedQueue_nextPosition(int);
void OrderedQueue_print(OrderedQueue);

bool Palindrome_test(OrderedQueue, OrderedStack);