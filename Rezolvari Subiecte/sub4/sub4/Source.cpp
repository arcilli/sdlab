#include <iostream>

typedef int atom;

using namespace std;

struct agenda
{
	char nume[100];
	char nr_telefon[100];
};

struct node
{
	agenda data;
	node* next;
};

void insert(node* &head)
{
	node *p = new node;
	cout << "insert name:";
	cin.getline(p->data.nume, 99);
	cout << "insert phone number: ";
	cin.get(p->data.nr_telefon, 99);
	if (nullptr == head || strcmp(p->data.nume, head->data.nume) <0)
	{
		p->next = head;
		head = p;
	}
	else
	{
		//cat timp sirul e mai mare decat primul, merg
		while (strcmp(p->data.nume, head->data.nume) >0 && nullptr != head->next)
		{
			head=head->next;
		}
		head->next=p;
	}
}

void print(const node* head)
{
	while (nullptr != head)
	{
		cout << head->data.nume << "\t" << head->data.nr_telefon;
		head = head->next;
		cout << endl;
	}
}

int main()
{
	node *head = nullptr;
	insert(head);
	cin.get();
	insert(head);
	cin.get();
	insert(head);

	print(head);
	return 0;
}