
2. Se citeste de la intrare un sir de valori numerice intregi, pe
o linie, separate de spatii, sir care se incheie cu o valoare 0.
     a) Sa se depuna elementele citite intr-o lista inlantuita
     neordonata (prin inserari repetate in fata).
     b) Sa se afiseze continutul listei.
     c) Se citeste o valoare intreaga si sa se determine daca
     aceasta se gaseste in lista.
     d) Se citeste o noua valoare intreaga X si N, pozitia in care
     aceasta valoare trebuie inserata in lista (al citelea element
     va fi elementul inserat). Sa se insereze valoarea X in pozitia
     N si sa se afiseze din nou lista.

EXEMPLU DE RULARE:
     Introduceti elementele listei: 5 3 7 10 4 9 1 0
     Am citit: 1 9 4 10 7 3 5
     Introduceti valoarea cautata: 9
     Valoarea 9 este in lista!
     Introduceti valoarea de inserat:2
     Introduceti pozitia: 3
     Lista completata: 1 9 3 4 10 7 3 5
=====================================================




#include <stdio.h>
#include <conio.h>
#include "lista.h"


void InitLista(Elem*& p)
{
	p = NULL;
}


void CitElemente(Elem*& p)
{
	int nr=0;
	char c;
	while( (c=getchar()) != '0' ){
		if( c > '0' && c <= '9' )
			nr=nr*10+c-'0';
		 else{
			InsertFata(p,nr);
			nr = 0;}}

}


void InsertFata(Elem*& p,int data)
{
	Elem* x;
	x = new Elem;
	x -> data = data;
	x -> link = p;
	p = x;
}


void AfisLista(Elem*& p)
{
	Elem* q;
	q = p;
	Test(p);
	while( q != NULL ){
		printf(" %d ",q->data);
		q=q->link;}

}


void Test(Elem* p)
{
	if ( p== NULL ) printf("\nSTIVA GOALA\a");
}

void CitData(Elem* p)
{
	Elem* x;
	int nr;
	printf("\nINTRODUCETI UN NUMAR INTREG : ");
	scanf("%d",&nr);
	if( Cauta(p,nr) == 0)
			printf("\nELEMENTUL %d SE AFLA IN LISTA\n",nr);
		 else
			printf("\nELEMENTUL %d NU SE AFLA IN LISTA\n",nr);

}

int Cauta(Elem* p,int data)
{
	Elem* q=p;
	Test(p);
	while( q != NULL ){
		if(q->data == data)
			return 0;
		 else
			q = q -> link;}
	return -1;
}

void CitVal(Elem* p)
{
	int nr,poz;
	Elem* q=p;
	Elem* x;

	x = new Elem;
	printf("\nINTRODUCETI UN NUMAR : ");
	scanf("%d",&nr);
	printf("\nINTRODUCETI POZITIA : ");
	scanf("%d",&poz);
	x -> data = nr;
	for(int i=1;i<poz-1;i++)
		q = q->link;
	x -> link = q -> link;
	q -> link = x;
}


#ifndef _LISTA_
#define _LISTA_

struct Elem{
	int data;
	Elem* link;
	};


void InitLista(Elem*& p);
void InsertFata(Elem*& p,int data);
void CitElemente(Elem*& p);
void AfisLista(Elem*& p);
void Test(Elem* p);
void CitData(Elem* p);
int Cauta(Elem* p,int data);
void CitVal(Elem* p);


#endif



#include <stdio.h>
#include <conio.h>
#include "lista.cpp"

void main()
{
	Elem* p;
	clrscr();

	InitLista(p);
	printf("INTRODUCETI ELEMENTELE LISTEI TERMINATE CU VAL. 0 \n");
	CitElemente(p);
	printf("AM CITIT \n");
	AfisLista(p);
	CitData(p);
	CitVal(p);
	printf("LISTA COMPLETA ESTE :\n");
	AfisLista(p);

	getch();
}





