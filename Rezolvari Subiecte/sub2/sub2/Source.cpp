#include <iostream>

typedef int atom;

using namespace std;

struct node
{
	atom data;
	node *next;
};

bool insert(node* &head, atom value)
{
	node* p = new node;
	p->data = value;
	p->next = head;
	head = p;
	return true;
}

bool search(const node* head, atom value)
{
	while (nullptr != head)
	{
		if (value == head->data)
		{
			return true;
		}
		head = head->next;
	}
	return false;
}

void print(const node* head)
{
	while (nullptr != head)
	{
		cout << head->data << "  ";
		head = head->next;
	}
}

void insertPoz(node* &head, atom value, atom position)
{

	if (position <=1)
	{
		insert(head, value);
	}
	else
	{
		position--;
		node*p = head;
		while (position > 1 && p->next)
		{
			p = p->next;
			position--;
		}
		node *q = new node;
		q->data = value;
		q->next = p->next;
		p->next = q;
	}
}

int main()
{
	int x;
	cout << "value: ";
	cin >> x;
	node* head = nullptr;
	while (x)
	{
		insert(head, x);
		cout << "value: ";
		cin >> x;
	}
	cout << endl;
	print(head);
	cout << endl << "value? :";
	cin >> x;
	(search(head, x)) ? cout << "da" : cout << "nu";

	cout << endl << endl << "value, poz";
	cin >> x;
	int poz;
	cin >> poz;
	insertPoz(head, x, poz);
	print(head);
}