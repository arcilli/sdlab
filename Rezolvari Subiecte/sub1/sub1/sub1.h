#pragma once

typedef int atom;

struct node
{
	atom data;
	node* next;
};

bool insert(node* &head, atom value);
void print(const node* head);
bool search(const node*head, atom value);
void sort(node* head);