#include <iostream>
#include "sub1.h"

using namespace std;

bool insert(node* &head, atom value)
{
	node* p = new node;
	p->data = value;
	if (nullptr == head)
	{
		//lista goala, pun primul element
		p->next = nullptr;
		head = p;
		return true;
	}
	else
	{
		p->next = head;
		head = p;
	}
	return false;
}

void print(const node* head)
{
	while (nullptr != head)
	{
		cout << head->data << "  ";
		head = head->next;
	}
}

bool search(const node* head, atom value)
{
	while (nullptr != head)
	{
		if (head->data == value)
		{
			return true;
		}
		head = head->next;
	}
	return false;
}

void sort(node* head)
{
	while (nullptr != head->next)
	{
		//iau fiecare si-l compar
		node* q = head->next;
		while (nullptr != q)
		{
			if (head->data > q->data)
			{
				atom aux = head->data;
				head->data = q->data;
				q->data = aux;
			}
			q = q->next;
		}
		head = head->next;
	}
}