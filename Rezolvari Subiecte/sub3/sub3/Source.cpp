#include <iostream>

using namespace std;

typedef int atom;

struct node
{
	atom data;
	node* next;
};

void insert(node* &head, atom value)
{
	node*p=new node;
	p->data=value;
	p->next=head;
	head=p;
}

void print(const node* head)
{
	while (nullptr != head)
	{
		cout<<head->data<< "  ";
		head=head->next;
	}
}

void stergere(node* &head, int pos)
{
	if (pos <=1 )
	{
		node*p=head;
		head=head->next;
		delete p;
	}
	else
	{
		node*p=head;
		pos-=2;
		while (pos && nullptr !=p->next)
		{
			p=p->next;
			pos--;
		}
		node *q=p->next;
		p->next=q->next;
		delete q;
	}
}

int main()
{
	int x;
	cout<<"value: ";
	cin>>x;
	node* head=nullptr;
	while (x)
	{
		insert(head, x);
		cout<<"value: ";
		cin>>x;
	}
	print(head);

	cout<<endl<<"pos: ";
	cin>>x;
	stergere(head, x);
	print(head);
	return 0;
}