
8. Fisierul GLISTA.H (interfata moduluilui GLISTA.CPP) contine:

/****************************************************************
					Fisierul GLISTA.H
*****************************************************************/
		 typedef char Atom;
		 enum { ATOM, LISTA};
		 struct GElement;		 // declaratie simpla
		 typedef GElement* GLista;	 // o lista generalizata este un
				 // pointer la primul element

		 struct GElement{
			char tag;		 // eticheta
			union {		 // data poate fi
		Atom A; 	 //	 Atom
		GLista L;	 //  sau GLista
		} data;
		 GElement* link;	  // legatura
		 };

GLista creareGLista();

Functia creareGLista() creeaza o lista generalizata si intoarce un
pointer la aceasta. Lista generalizata este creata pornind de la o
descriere cu paranteze introdusa de la tastatura, de forma indicata
de urmatoarele diagrame de sintaxa:
			   旼컴컴컴컴컴컴�컴컴컴컴컴커
     			   �	                     �
	 Lista     �袴敲   �	   旼컴컴컴컴�       �   �袴敲
	 컴컴컴컴�캘 ( 픔컴좔컴쩡�캑 Element 쳐컴쩡�컨컴캘 ) 픔캄컴�
  		   훤袴�       �   읕컴컴컴컴�   �	 훤袴�
			       �      �袴敲	 �
			       읕컴�컴� , 픔컴�컴�
			              훤袴�
			    컴컴컴컴커
               	    旼캄컴�	Atom 쳐컴커
	 Element    �     읕컴컴컴컴켸    �
	 컴컴컴컴�컴                      쳐컴캄컴�
        	    �     旼컴컴컴컴커    �
		     컴�컴� Lista    쳐컴켸
        	   	  읕컴컴컴컴켸

Exemple: (A,(B,C),(D))	 - O lista formata dintr-un atom A, o
			 lista cu doua elemente (B,C) si o lista
			 cu un element (D) ;
	 ((A,B,C),(),B)  - O lista formata dintr-o lista cu trei
			 elemente (A,B,C), o lista vida () si un
			 atom B .
Spatiile inserate in sirul de caractere introdus de la tastatura
sint ignorate. Se cere:
     a) Sa se afiseze toti atomii din lista.
     b) Sa se determine atomul cu valoarea cea mai mica dintre
	atomii cuprinsi in lista citita si in sublistele ei.
     c) Sa se citeasca o a doua liste si sa se testeze daca cele
	doua liste sint egale. Doua liste sint egale daca elementele lor
	luate in ordine sint egale doua cite doua.

EXEMPLU DE RULARE:
     Introduceti lista: (F,(T,(C)),(),(D,E))
     Atomii: F T C D E
     Cel mai mic: C
     Introduceti a doua lista: (F,(T,C),(),(D,E))
     Nu sint egale!

=====================================================



/****************************************************************
                      Fisierul GLISTA.CPP
*****************************************************************/
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <ctype.h>

#include "GLISTA.H"

char crtChar;

void eroare()
{
     printf("Sirul de intrare este eronat!\n"
             "Apasati o tasta ...");
     getch();
     exit(1);
}

void readchar()
{
     do crtChar=getchar();
     while(crtChar==' ');
}

char citesteAtom()
{
     char c;
     if(!isalpha(crtChar)) eroare();
     c = crtChar;
     readchar();
     return c;
}

GLista citesteGLista();

GElement* citesteGElement()
{
     GElement* p = new GElement;
     if(crtChar=='(') {
		      p->data.L = citesteGLista();
		      p->tag = LISTA;
		      }
     else {
	  p->data.A = citesteAtom();
		p->tag = ATOM;
		}
		p->link = NULL;
		return p;
}

GLista citesteGLista()
{
		 GElement* cap, *coada, *p;
		 if( crtChar!='(' ) eroare();
		 readchar();
		 if( crtChar==')' )  cap = NULL;
		 else {
		cap = citesteGElement();
		coada = cap;
		while( crtChar==',' ) {
		readchar();
								p = citesteGElement();
								coada->link = p;
								coada = p;
								}
					if( crtChar!=')' ) eroare();
					}
		 readchar();
		 return cap;
}

GLista CreareGLista()
{
		 do readchar();
		 while (crtChar=='\n');
		 return (citesteGLista());
}

void 	AfisAtomi (GLista v)
{
	GElement* p;
	p = v;
	while (p != NULL )
	{
		if ( p->tag == ATOM )
			printf(" %c ",p->data.A);
		 else
			AfisAtomi(p->data.L);
		p = p -> link;
	}
}

char 	AtomValMin (GLista v)
{
	GElement* p;
	char min;
	p = v;
	min = p->data.A;
	while (p != NULL )
	{
		if ( p->tag == ATOM )
		{
			if ( p->data.A < min )
				min = p->data.A;
		}
		 else
			 min = AtomValMin(p->data.L);
		p = p -> link;
	}

	return min;
}

int EgalitateListe (GLista v1,GLista v2)
{
	GElement* p1;
	GElement* p2;
	int cond = 0;
	p1 = v1;
	p2 = v2;


	while ( p1 != NULL && p2 != NULL && cond == 0 )
	{
		cond = 1;
		if( p1 -> tag == ATOM && p2 -> tag == ATOM )
		{
			if ( p1 -> data.A == p2 -> data.A )
				cond = 0;
		}
	 else
	 {
			cond = EgalitateListe(p1->data.L,p2->data.L);
	 }
		p1 = p1 -> link;
		p2 = p2 -> link;
	}

	if (p1 == NULL && p2 == NULL && cond == 0 )
		return 0;
	 else
		return -1;
}


****************************************************************
                      Fisierul GLISTA.H
*****************************************************************/
     typedef char Atom;
     enum { ATOM, LISTA};
     struct GElement;            // declaratie simpla
     typedef GElement* GLista;   // o lista generalizata este un
                                 // pointer la primul element 

     struct GElement{
            char tag;            // eticheta
            union {              // data poate fi
                Atom A;          //      Atom
                GLista L;        //  sau GLista
                } data;
	   GElement* link;        // legatura
           };                               

GLista creareGLista();
     // citeste si returneaza o lista generalizata data de la
     // intrarea standard, respectind sintaxa specificata.



#include <stdio.h>
#include <conio.h>
#include "glista.cpp"

void main()
{
	GElement* cap1;
	GElement* cap2;
	char opt;

	do{

		clrscr();

		printf("\nINTRODUCETI PRIMA LISTA\n");
		cap1 = CreareGLista();
		AfisAtomi(cap1);
		printf("\nATOMUL CU VAL MIN : %c \n",AtomValMin(cap1));
		printf("\nINTRODUCETI A-II-A LISTA \n");
		cap2 = CreareGLista();
		if( EgalitateListe(cap1,cap2) == 0)
			printf("LISTELE INTRODUSE SINT EGALE ");
		 else
			printf("\nLISTELE INTRODUSE NU SINT EGALE \a");

		 printf("\n\n\t\t\tRELUAM ? (Y/N)");
		 opt = getche();

	} while( opt == 'y' || opt == 'Y' );
	getch();
}