#include <iostream>

using namespace std;

int j=0;

struct Nod {
	char data;
	Nod* stg, *drt;
};
Nod* creareArbore();

char car;

void eroare()
{
	printf("Sirul de intrare este eronat!\n");
	printf("Apasati tasta o tasta...");
	exit(1);
}

char readchar()
{
	char c;
	do  c = getchar();  while (c == ' ');
	return c;
}

char citesteNume()
{
	char c;
	if (!isalpha(car)) eroare();
	c = car;
	car = readchar();
	return c;
}

Nod* citesteArbore()
{
	Nod* rad;
	if (car == '-') {
		rad = 0;
		car = readchar();
	}
	else {
		rad = static_cast<Nod*>(malloc(sizeof(Nod)));
		rad->data = citesteNume();
		if (car != '(') {
			rad->stg = 0;
			rad->drt = 0;
		}
		else {
			car = readchar();
			rad->stg = citesteArbore();
			if (car != ',')    rad->drt = 0;
			else {
				car = readchar();
				rad->drt = citesteArbore();
			}
			if (car != ')')  eroare();
			car = readchar();
		}
	}
	return rad;
}

Nod* creareArbore()
{
	printf("\nIntroduceti arborele:");
	car = readchar();
	return citesteArbore();
}

void in_order_print(const Nod* head, char aux[])
{
	if (nullptr != head)
	{
		in_order_print(head->stg, aux);
		cout<<head->data<<"  ";
		aux[j++]=head->data;
		in_order_print(head->drt, aux);
	}
}

int main()
{
	Nod* head=creareArbore();
	char aux[100];
	in_order_print(head, aux);
	cout<<endl<<endl;
	for (int i=0; i<j; ++i)
	{
		cout<<aux[i]<<"  ";
	}
	return 0;
}