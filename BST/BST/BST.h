#pragma once

typedef int ATOM;
#define DIMMAX 100

struct NODE_t
{
	ATOM data;
	NODE_t *left, *right;
};

bool BinarySearchTree_insert(NODE_t* &head, ATOM value);
void BinarySearchTree_inOrderTraversal(const NODE_t* head);
void BinarySearchTree_preOrderTraversal(const NODE_t* head);
void BinarySearchTree_postOrderTraversal(const NODE_t* head);
void BinarySearchTree_indentPrint(const NODE_t* head, int level);

/*
ATOM BinarySearchTree_getMinValue(const NODE_t* head);
ATOM BinarySearchTree_getMaxValue(const NODE_t* head);
*/

int BinarySearchTree_noLeafs(const NODE_t* head);
int BinarySearchTree_noNodes(const NODE_t* head);
int BinarySearchTree_noInternalNodes(const NODE_t* head);
int BinarySearchTree_depth(const NODE_t* head);
void BinarySearchTree_rotate(NODE_t* head);
void BinarySerachTree_bracketPrint(const NODE_t* head);


//queue part
void BinarySearchTree_inLine_Traversal(NODE_t* head);
struct OrderedQueue_t
{
	int head, tail;
	NODE_t* array[DIMMAX];
};

void OrderedQueue_init(OrderedQueue_t& variable);
int OrderedQueue_nextPosition(int);
NODE_t* OrderedQueue_get(OrderedQueue_t& variable);
bool OrderedQueue_put(OrderedQueue_t& variable, NODE_t* aux);
bool OrderedQueue_isEmpty(OrderedQueue_t);
bool OrderedQueue_isFull(OrderedQueue_t);

/*
 *stack part
 */
struct OrderedStack_t
{
	NODE_t* array[DIMMAX];
	int stackPointer;
};

void StackInit(OrderedStack_t& aux);
bool StackPush(OrderedStack_t& aux, NODE_t* val);
NODE_t* StackPop(OrderedStack_t& aux);
bool StackIsEmpty(OrderedStack_t aux);
bool StackIsFull(OrderedStack_t aux);

void BinarySearchTree_iterativeInOrderTraversal(NODE_t* head);

/*
 *Polish form
 */

//constructie arbore binar aferent expresiei in forma poloneza
NODE_t* BuildTree(char *expr);

/*constructie functie care evalueaza expresie aritmetica sub forma poloneza
 *si implementata printr-un arbore binar, in care operanzii sunt frunze
 *
 *
 *PT CARACTER->CIFRA: SCAD '0'
 */
int Evaluate(NODE_t* head);


/*
 **********************************************Specific BST functions*********************************
 */

//sa se scrie o functie nerecursiva care intoarce  pointer la un nod de cheie data dintr-un BST
NODE_t* BinarySearchTree_search(NODE_t* head, ATOM key);

//sa se scrie o functie NERECURSIVA care intoarce  pointer la un nod de cheie data dintr-un BST
NODE_t* BinarySearchTree_searchIterative(NODE_t*head, ATOM key);

//sa se scrie o functie care intoarce pointer la parintele unui nod de cheie data
NODE_t* BinarySearchTree_fatherOf(NODE_t* head, NODE_t* r);

//sa se scrie o functie care intoarce pointer la succesorul (intr-o parcrugere in inordine) a unui nod
//de cheie data

//!!!!!!!!!!!!!!!!!!!IMPLEMENT THIS!!!!!!!!!!!!!!
//
//
//NODE_t* BinarySearchTree_NextNode(NODE_t* head, ATOM key);

/*
 *delete functions for bst
 */

//stergerea unui nod oarecare
void BinarySearchTree_deleteNode(NODE_t*& head, ATOM key);
void BinarySearchTree_deleteRoot(NODE_t* &head);
NODE_t* BinarySearchTree_removeGreatest(NODE_t* &head);

//minim value from BST
NODE_t* BinarySearchTree_minValue(NODE_t* const head);

//maxim value from BST
NODE_t* BinarySearchTree_maxValue(NODE_t* const head);

//check if its a BST or not
void BinarySearchTree_checkIsBST(const NODE_t* head, ATOM array1[150]);

bool isAVL(NODE_t* head);