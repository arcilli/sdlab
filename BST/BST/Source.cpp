#include <iostream>
#include "BST.h"
#include "Randomize.h"

using namespace std;

int main()
{
	

	/*
	 * used here char instead of in in typedef
	 */
	/*
	NODE_t* head=BuildTree("12*34+23+*+");
	BinarySearchTree_indentPrint(head,0);
	int eva=Evaluate(head);
	cout<<"Evaluation:" <<eva;
	*/
	int n;
	cout << "n: ";
	cin >> n;
	ATOM vector[100];
	for (auto i = 1; i < n+1; ++i)
	{
		vector[i-1] = i;
	}
	randomize(vector, n);
	NODE_t* head = nullptr;
	cout << "Initial vector: ";
	for (auto i = 0; i < n; ++i)
	{
		cout << vector[i] << "  ";
	}
	for (auto i=0; i<n; ++i)
	{
		if (!BinarySearchTree_insert(head, vector[i]))
		{
			throw exception("Cant be inserted?!?");
		}
	}


	cout << endl;
	BinarySearchTree_inOrderTraversal(head);
	cout << endl;
	BinarySearchTree_indentPrint(head, 1);
	bool x=isAVL(head);
	cout<<x;

	///*cout<<"Nr. of leafs: "<<BinarySearchTree_noLeafs(head)<<endl;
	//cout<<"Nr. of nodes: "<<BinarySearchTree_noNodes(head);
	//cout<<endl<<"Nr. of internal nodes: "<<BinarySearchTree_noInternalNodes(head);
	//cout<<endl<<"Reversing the tree:"<<endl;
	//BinarySearchTree_rotate(head);
	//BinarySearchTree_indentPrint(head, 1);
	//cout<<endl<<"Depth: "<<BinarySearchTree_depth(head);*/

	///*BinarySerachTree_bracketPrint(head);
	//cout<<"inline, traversal"<<endl;*/
	//BinarySearchTree_inLine_Traversal(head);

	///*cout<<"iterative, inOrder traversal";
	//BinarySearchTree_iterativeInOrderTraversal(head);*/

	////ATOM key;
	////cout<<"key?";
	////cin>>key;
	////NODE_t* aux=BinarySearchTree_search(head, key);
	////(aux)? cout<<aux->data : cout<<"NULL?!?";


	///*cout<<endl<<"TESTING DELETE: ";
	//ATOM key;
	//cout<<"key?";
	//cin>>key;
	//BinarySearchTree_deleteNode(head, key);
	//BinarySearchTree_indentPrint(head, 1);*/

	//cout<<"Minim value: "<<BinarySearchTree_minValue(head)->data;
	//cout<<"Maxim value: "<<BinarySearchTree_maxValue(head)->data;

	//cout<<"Testing value";
	//ATOM key;
	//cout<<"key?";
	//cin>>key;

	////working fine, just need to check if its a null pointer
	//cout<<BinarySearchTree_searchIterative(head, key)->data;

	////this is working
	//cout<<BinarySearchTree_fatherOf(head, head->left->right)->data;

	/*extern int j;
	extern ATOM array1[150];
		head->data=4;
	BinarySearchTree_checkIsBST(head, array1);
	cout<<endl;

	int ok=1;
	for (int i=0; i<j; ++i)
	{
		if (array1[i]>array1[i+1])
		{
			cout<<"NU E BST";
			ok=0;
			break;
		}
	}
	if (ok)
	{
		cout<<"Bst";
	}*/
	return 0;
}