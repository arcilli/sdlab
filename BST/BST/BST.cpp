#include <iostream>
#include "BST.h"

using namespace std;

int j=-1;
ATOM array1[150];

bool BinarySearchTree_insert(NODE_t* &head, ATOM value)
{
	NODE_t* p = new NODE_t;
	p->data = value;
	if (nullptr == head)
	{
		head = p;
		p->right = p->left = nullptr;
		return true;
	}
	else
	{
		if (value > head->data)
		{
			return BinarySearchTree_insert(head->right, value);
		}
		else
		{
			if (value < head->data)
			{
				return BinarySearchTree_insert(head->left, value);
			}
			return false;
		}
	}
}

void BinarySearchTree_inOrderTraversal(const NODE_t* head)
{
	//left, root, right
	if (nullptr != head)
	{
		BinarySearchTree_inOrderTraversal(head->left);
		cout << head->data << "  ";
		BinarySearchTree_inOrderTraversal(head->right);
	}
}


void BinarySearchTree_preOrderTraversal(const NODE_t* head)
{
	if (nullptr != head)
	{
		cout << head->data << "  ";
		BinarySearchTree_preOrderTraversal(head->left);
		BinarySearchTree_preOrderTraversal(head->right);
	}
}

void BinarySearchTree_postOrderTraversal(const NODE_t* head)
{
	if (nullptr != head)
	{
		BinarySearchTree_postOrderTraversal(head->left);
		BinarySearchTree_postOrderTraversal(head->right);
		cout << head->data;
	}
}

void BinarySearchTree_inLine_Traversal(NODE_t* head)
{
	OrderedQueue_t cda{};
	if (nullptr != head)
	{
		OrderedQueue_init(cda);
		OrderedQueue_put(cda, head);
		while (!OrderedQueue_isEmpty(cda))
		{
			NODE_t *p = OrderedQueue_get(cda);
			cout << p->data << "  ";
			if (nullptr != p->left)
			{
				OrderedQueue_put(cda, p->left);
			}
			if (nullptr != p->right)
			{
				OrderedQueue_put(cda, p->right);
			}
		}
	}
}

void BinarySearchTree_indentPrint(const NODE_t* head, const int level)
{
	for (auto i = 0; i < level; ++i)
	{
		cout << "\t";	//am pus cate taburi trebuie
	}
	if (nullptr == head)
	{
		cout << "-" << endl;
	}
	else
	{
		cout << head->data << endl;
		BinarySearchTree_indentPrint(head->left, level + 1);
		BinarySearchTree_indentPrint(head->right, level + 1);
	}

}

void BinarySerachTree_bracketPrint(const NODE_t* head)
{
	if (nullptr != head)
	{
		cout << head->data << "(";
		BinarySerachTree_bracketPrint(head->left);
		cout << ",";
		BinarySerachTree_bracketPrint(head->right);
		cout << ")";
	}
	else
	{
		cout << "-"<<endl;
	}
}
/*
ATOM BinarySearchTree_getMinValue(const NODE_t* head)
{

}

ATOM BinarySearchTree_getMaxValue(const NODE_t* head)
{
	return 0;
}

*/


int BinarySearchTree_noLeafs(const NODE_t* head)
{
	if (nullptr == head)
	{
		return 0;
	}
	if (nullptr == head->left && nullptr == head->right)
	{
		return 1;
	}
	return BinarySearchTree_noLeafs(head->left) + BinarySearchTree_noLeafs(head->right);
}

int BinarySearchTree_noNodes(const NODE_t* head)
{
	if (nullptr == head)
	{
		return 0;
	}
	return 1 + BinarySearchTree_noNodes(head->left) + BinarySearchTree_noNodes(head->right);
}

int BinarySearchTree_noInternalNodes(const NODE_t* head)
{
	if (nullptr == head)
	{
		return 0;
	}
	if (nullptr == head->left && nullptr == head->right)
	{
		return 0;
	}
	return 1 + BinarySearchTree_noInternalNodes(head->left) + BinarySearchTree_noInternalNodes(head->right);
}

int BinarySearchTree_depth(const NODE_t* head)
{
	auto ldepth = 1, rdepth = 1;
	if (nullptr == head)
	{
		return 0;
	}
	ldepth += BinarySearchTree_depth(head->left);
	rdepth += BinarySearchTree_depth(head->right);
	return ((ldepth > rdepth) ? ldepth : rdepth);
}

void BinarySearchTree_rotate(NODE_t* head)
{
	if (nullptr != head)
	{
		auto* p = head->left;
		head->left = head->right;
		head->right = p;
		BinarySearchTree_rotate(head->right);
		BinarySearchTree_rotate(head->left);
	}
}

void OrderedQueue_init(OrderedQueue_t& variable)
{
	variable.head = 0;
	variable.tail = 0;
}

int OrderedQueue_nextPosition(int aux)
{
	if (aux < DIMMAX - 1)
	{
		aux++;
		return aux;
	}
	else
	{
		return 0;
	}
}

NODE_t* OrderedQueue_get(OrderedQueue_t& variable)
{
	if (!OrderedQueue_isEmpty(variable))
	{
		variable.head = OrderedQueue_nextPosition(variable.head);
		return variable.array[variable.head - 1];
	}
	throw exception("Coada goala");
}

bool OrderedQueue_put(OrderedQueue_t& variable, NODE_t* aux)
{
	if (!OrderedQueue_isFull(variable))
	{
		variable.array[variable.tail] = aux;
		variable.tail = OrderedQueue_nextPosition(variable.tail);
		return true;
	}
	return false;
}

bool OrderedQueue_isEmpty(OrderedQueue_t a)
{
	return (a.head == a.tail);
}

bool OrderedQueue_isFull(OrderedQueue_t a)
{
	return (OrderedQueue_nextPosition(a.tail) == a.head);
}

void StackInit(OrderedStack_t& aux)
{
	aux.stackPointer = -1;
}

bool StackPush(OrderedStack_t& aux, NODE_t* val)
{
	if (StackIsFull(aux))
	{
		return false;
	}
	aux.stackPointer++;
	aux.array[aux.stackPointer] = val;
	return true;
}

NODE_t* StackPop(OrderedStack_t& aux)
{
	if (!StackIsEmpty(aux))
	{
		aux.stackPointer--;
		return aux.array[aux.stackPointer + 1];
	}
	return nullptr;
}

bool StackIsEmpty(const OrderedStack_t aux)
{
	return (-1 == aux.stackPointer);
}

bool StackIsFull(const OrderedStack_t aux)
{
	return (DIMMAX - 1 == aux.stackPointer);
}

void BinarySearchTree_iterativeInOrderTraversal(NODE_t* head)
{
	OrderedStack_t stck{};
	NODE_t *p = head;
	StackInit(stck);
	while (nullptr != p || !StackIsEmpty(stck))
	{
		if (nullptr != p)
		{
			StackPush(stck, p);
			p = p->left;
		}
		else
		{
			p = StackPop(stck);
			cout << p->data << "  ";
			p = p->right;
		}
	}
}

NODE_t* BuildTree(char *expr)
{
	OrderedStack_t stck{};
	StackInit(stck);
	//Stiva de pointeri
	//Stiva va contine pointerii la radacinile subarborilor

	char ch;
	NODE_t* r = nullptr;
	while ((ch = *expr++))
	{
		r = new NODE_t;
		r->data = ch;
		if (isdigit(ch))
		{
			//Este numar -> este frunza (este un operand)
			r->left = r->right = nullptr;
		}
		else
		{
			//sunt pe o operatie
			r->left = StackPop(stck);
			r->right = StackPop(stck);
		}
		StackPush(stck, r);
	}
	return r;
}

int Evaluate(NODE_t* head)
{
	//semnifica valoarea exppresiei subarborelui stang & valoarea expresiei in subarborele drept
	//in orice situatie
	if (nullptr == head)
	{
		return 0; //conditie de oprire recursivitate
	}
	if (isdigit(head->data))
	{
		//sunt pe o frunza, adica pe un numar
		return head->data - '0';
	}
	const auto valS = Evaluate(head->left);
	const auto valD = Evaluate(head->right);
	//stiu sigur ca sunt pe un operator
	const char ch = head->data;
	switch (ch)
	{
	case '+':
		return valS + valD;
	case '*':
		return valS*valD;
	default:
		throw exception("Eroare");
	}
}

NODE_t* BinarySearchTree_search(NODE_t* head, const ATOM key)
{
	if (nullptr == head || head->data == key)
	{
		return head;
	}
	if (head->data < key)
	{
		return BinarySearchTree_search(head->right, key);
	}
	return BinarySearchTree_search(head->left, key);
}

void BinarySearchTree_deleteNode(NODE_t* &head, const ATOM key)
{
	if (nullptr != head)
	{
		if (key == head->data)
		{
			BinarySearchTree_deleteRoot(head);
		}
		else
		{
			if (key > head->data)
			{
				BinarySearchTree_deleteNode(head->right, key);
			}
			else
				BinarySearchTree_deleteNode(head->left, key);
		}
	}
}

void BinarySearchTree_deleteRoot(NODE_t* &head)
{
	if (nullptr == head->right)
	{
		auto* p = head;
		head = head->left;
		delete p;
	}
	else
	{
		if (nullptr == head->left)
		{
			auto*p = head;
			head = head->right;
			delete p;
		}
		else
		{
			auto* p = BinarySearchTree_removeGreatest(head->left);
			head->data = p->data;
			delete p;
		}
	}
}

NODE_t* BinarySearchTree_removeGreatest(NODE_t* &head)
{
	if (nullptr == head->right)
	{
		auto* p = head;
		head = head->left;
		return p;
	}
	return BinarySearchTree_removeGreatest(head->right);
}


NODE_t* BinarySearchTree_minValue(NODE_t* const head)
{
	if (nullptr == head->left)
	{
		return head;
	}
	return BinarySearchTree_minValue(head->left);
}

NODE_t* BinarySearchTree_maxValue(NODE_t* const head)
{
	if (nullptr == head->right)
	{
		return head;
	}
	return BinarySearchTree_maxValue(head->right);
}

NODE_t* BinarySearchTree_searchIterative(NODE_t*head, const ATOM key)
{
	while (nullptr != head && head->data != key)
	{
		if (key > head->data)
		{
			head = head->right;
		}
		else
		{
			head = head->left;
		}
	}
	return head;
}

NODE_t* BinarySearchTree_fatherOf(NODE_t* head, NODE_t* r)
{
	NODE_t *p = nullptr;
	if (nullptr == head || head == r)
	{
		return p;
	}
	auto *q = head;
	while (q != r)
	{
		p = q;
		if (q->data < r->data)
		{
			q = q->right;
		}
		else
		{
			q = q->left;
		}
	}
	return p;
}

//NODE_t* BinarySearchTree_NextNode(NODE_t* head, ATOM key)
//{
//	NODE_t *p;
//	NODE_t *r = BinarySearchTree_search(head, key);
//	if (nullptr ==r)
//	{
//		return r;
//	}
//	if (nullptr != r->right)
//	{
//	}
//}

void BinarySearchTree_checkIsBST(const NODE_t* head, ATOM array1[150])
{
	if (nullptr != head)
	{
		BinarySearchTree_checkIsBST(head->left, array1);
		array1[++j]=head->data;
		BinarySearchTree_checkIsBST(head->right, array1);
	}
}

bool isAVL(NODE_t* head)
{
	if (head)
	{
		if (abs(BinarySearchTree_depth(head->left) - BinarySearchTree_depth(head->right)) > 1)
		{
			return false;
		}
		isAVL(head->left); 
		isAVL(head->right);
	}
	return true;
}