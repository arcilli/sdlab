#include <cstdlib>
#include <ctime>
#include "Randomize.h"
#include "BST.h"

// A utility function to swap to integers
void swap(ATOM *a, ATOM *b)
{
	auto temp = *a;
	*a = *b;
	*b = temp;
}

// A function to generate a random permutation of arr[]
void randomize(ATOM arr[], int n)
{
	// Use a different seed value so that we don't get same
	// result each time we run this program
	srand(time(nullptr));

	// Start from the last element and swap one by one. We don't
	// need to run for the first element that's why i > 0
	for (auto i = n - 1; i >0; i--)
	{
		// Pick a random index from 0 to i
		auto j = rand() % (i + 1);

		// Swap arr[i] with the element at random index
		swap(&arr[i], &arr[j]);
	}
}