#pragma once

#define DIMMAX 100
typedef int ATOM;

struct OrderedQueue
{
	int capacity;
	int head, tail;
	ATOM* data;
};

//initializate the queue
void OrderedQueue_init(OrderedQueue&);
//get, 
bool OrderedQueue_isFull(const OrderedQueue);
ATOM OrderedQueue_top(const OrderedQueue);
void OrderedQueue_init(OrderedQueue&);
int nextPosition(int index);
bool OrderedQueue_isEmpty(const OrderedQueue a);
bool OrderedQueue_put(OrderedQueue &a, ATOM value);
void OrderedQueue_print(OrderedQueue a);
ATOM OrderedQueue_get(OrderedQueue& a);