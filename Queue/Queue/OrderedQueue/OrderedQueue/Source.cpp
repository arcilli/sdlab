#include <iostream>
#include "OrderedQueue.h"

using namespace std;

int main(void)
{
	int x;
	OrderedQueue queue;
	OrderedQueue_init(queue);
	cout << "value: ";
	cin >> x;
	while (x)
	{
		OrderedQueue_put(queue, x);
		cout << "value: ";
		cin >> x;
	}
	OrderedQueue_print(queue);
	cout << endl;
	cout << OrderedQueue_top(queue);
	cout << endl;
	OrderedQueue_get(queue);
	OrderedQueue_print(queue);
}