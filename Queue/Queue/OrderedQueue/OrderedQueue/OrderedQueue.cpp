#include <iostream>
#include "OrderedQueue.h"

using namespace std;

int nextPosition(int index)
{
	if (index < DIMMAX-1)
	{
		index++;
	}
	else
	{
		index = 0;
	}
	return index;
}
bool OrderedQueue_isFull(const OrderedQueue cda)
{
	return (nextPosition(cda.tail) == cda.head);
}

void OrderedQueue_init(OrderedQueue& cda)
{
	cda.head = cda.tail = 0;
	cda.capacity = DIMMAX;
	cda.data = new ATOM[DIMMAX];
}

bool OrderedQueue_isEmpty(const OrderedQueue a)
{
	return (a.head == a.tail);
}

bool OrderedQueue_put(OrderedQueue &a, ATOM value)
{
	if (OrderedQueue_isFull(a))
	{
		//coada este plina
		cout << "Coada plina";
		return false;
	}
	else
	{
		a.data[a.tail] = value;
		a.tail = nextPosition(a.tail);
		return true;
	}
}

void OrderedQueue_print(OrderedQueue a)
{
	while (a.head != a.tail)
	{
		cout << a.data[a.head]<<"  ";
		a.head =  nextPosition(a.head);
	}
}

ATOM OrderedQueue_get(OrderedQueue& a)
{
	ATOM aux;
	if (OrderedQueue_isEmpty(a))
	{
		cout << "Empty list";
		return 0;
	}
	else
	{
		aux = a.data[a.head];
		a.head = nextPosition(a.head);
	}
	return aux;
}

ATOM OrderedQueue_top(const OrderedQueue a)
{
	if (OrderedQueue_isEmpty(a))
	{
		cout << "empty list";
		return 0;
	}
	else
	{
		return a.data[a.head];
	}
}