#include <iostream>
#include "LinkedQueue.h"

using namespace std;

int main(void)
{
	LinkedQueue lst;
	LinkedQueue_init(lst);
	int value;
	cout << "value: ";
	cin >> value;
	while (value)
	{
		LinkedQueue_put(lst, value);
		cout << "value: ";
		cin >> value;
	}
	LinkedQueue_print(lst);
	cout << LinkedQueue_front(lst)<<endl;
	cout<< LinkedQueue_get(lst);
	return 0;
}