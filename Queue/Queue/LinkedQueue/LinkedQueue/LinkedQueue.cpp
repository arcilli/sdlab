#include <iostream>
#include "LinkedQueue.h"

using namespace std;

void LinkedQueue_put(LinkedQueue& lst, ATOM val)
{
	NODE_t *p=new NODE_t;
	p->data = val;
	p->next = nullptr;
	if (LinkedQueue_isEmpty(lst))
	{
		lst.tail = p;
		lst.head = p;
	}
	else
	{
		lst.tail->next = p;
		lst.tail = p;
	}
}

void LinkedQueue_init(LinkedQueue& lst)
{
	lst.head = lst.tail = nullptr;
}

ATOM LinkedQueue_front(const LinkedQueue lst)
{
	if (!LinkedQueue_isEmpty(lst))
	{
		//am ce sa scot
		return lst.tail->data;
	}
	else
	{
		cout << "Empty list";
		return 0;
	}
}

bool LinkedQueue_isEmpty(const LinkedQueue lst)
{
	return (nullptr == lst.head);
}

ATOM LinkedQueue_get(LinkedQueue& lst)
{
	NODE_t *p;
	int a=0;
	if (LinkedQueue_isEmpty(lst))
	{
		cout << "Empty list, cant execute.";
	}
	else
	{
		p = lst.head;
		lst.head = p->next;
		a = p->data;
		delete p;
		p = nullptr;
		return a;
	}
}
//
void LinkedQueue_print(LinkedQueue lst)
{
	if (LinkedQueue_isEmpty(lst))
	{
		cout << "Empty list";
		//_ASSERT(0);
	}
	else
	{
		while (lst.head != lst.tail)
		{
			cout << lst.head->data << "  ";
			lst.head = lst.head->next;
		}
		cout << lst.head->data << endl;
	}
}