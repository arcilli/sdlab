#pragma once

typedef int ATOM;
struct NODE_t
{
	ATOM data;
	NODE_t* next;
};

struct LinkedQueue
{
	NODE_t *head, *tail;
};

void LinkedQueue_put(LinkedQueue&, ATOM);
void LinkedQueue_init(LinkedQueue&);
ATOM LinkedQueue_front(const LinkedQueue);
bool LinkedQueue_isEmpty(const LinkedQueue);
ATOM LinkedQueue_get(LinkedQueue&);
void LinkedQueue_print(LinkedQueue);