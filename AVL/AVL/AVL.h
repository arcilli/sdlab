#pragma once

typedef int ATOM;

struct node_t
{
	ATOM data;
	node_t *left, *right;
	int balance_factor;
};

void rotatie_dubla_dreapta(node_t* &a);
void InsAVL(node_t* &a, ATOM x);


void rotatie_simpla_stanga(node_t *&a);
void rotatie_simpla_dreapta(node_t *&a);

bool isAVL(node_t* head);
int depth(node_t* head);