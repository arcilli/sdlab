#include <iostream>
#include "AVL.h"

using namespace std;

int eq_required;

void rotatie_dubla_dreapta(node_t* &a)
{
	node_t* b, *c;
	b=a->left;
	c=b->right;
	//se actualizeaza balance factor pentru starea finala
	switch (c->balance_factor)
	{
	case 0:
		a->balance_factor=b->balance_factor=0;
		break;
	case -1:
		a->balance_factor=0;
		b->balance_factor=1;
		break;
	case 1:
		a->balance_factor=-1;
		b->balance_factor=0;
	}
	a->left=c->right;
	b->right=c->left;
	c->balance_factor=0;
	c->right=a;
	c->left=b;
	a=c;
}

void rotatie_dubla_stanga(node_t* &a)
{
	node_t* b, *c;
	b=a->right;
	c=b->left;
	//se actualizeaza balance factor pentru starea finala
	switch (c->balance_factor)
	{
	case 0:
		a->balance_factor=b->balance_factor=0;
		break;
	case 1:
		a->balance_factor=0;
		b->balance_factor=1;
		break;
	case -1:
		a->balance_factor=-1;
		b->balance_factor=0;
	}
	a->right=c->left;
	b->left=c->right;
	c->balance_factor=0;
	c->left=a;
	c->right=b;
	a=c;
}

void rotatie_dubla_dreapta(node_t* &a)
{
	node_t* b, *c;
	b=a->left;
	c=b->right;
	//se actualizeaza balance factor pentru starea finala
	switch (c->balance_factor)
	{
	case 0:
		a->balance_factor=b->balance_factor=0;
		break;
	case -1:
		a->balance_factor=0;
		b->balance_factor=1;
		break;
	case 1:
		a->balance_factor=-1;
		b->balance_factor=0;
	}
	a->left=c->right;
	b->right=c->left;
	c->balance_factor=0;
	c->right=a;
	c->left=b;
	a=c;
}

void rotatie_simpla_stanga(node_t *&a)
{
	node_t *b=a->right;
	a->right=b->left;
	b->left=a;
	if (b->balance_factor == -1)
	{
		a->balance_factor=b->balance_factor=0;
	}
	a=b;
}

void rotatie_simpla_dreapta(node_t *&a)
{
	node_t *b=a->left;
	a->left=b->right;
	b->right=a;

	if (b->balance_factor == 1)
	{
		a->balance_factor=b->balance_factor=0;
	}
	a=b;
}

bool isAVL(node_t* head)
{
	if (head)
	{
		if (abs(depth(head->left) - depth(head->right)) > 1)
		{
			return false;
		}
		isAVL(head->left);
		isAVL(head->right);
	}
	return true;
}

int depth(node_t* head)
{
	int a=1, b=1;
	if (nullptr == head->left && nullptr == head->right)
	{
		return 0;
	}
	a+=depth(head->left);
	a+=depth(head->right);
	return ((a>b)? a: b);
}