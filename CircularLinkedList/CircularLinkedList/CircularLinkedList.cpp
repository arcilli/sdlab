#include <iostream>
#include "CircularLinkedList.h"

using namespace std;

void CircularLinkedList_create(NODE_t*& head)
{
	NODE_t *p;
	int value;
	cout << "value: ";
	cin >> value;
	while (value)
	{
		p = new NODE_t;
		p->data = value;
		if (nullptr == head)
		{
			head = p;
			p->next = p;
		}
		else
		{
			p->next = head->next;
			head->next = p;
			head = p;
		}
		cout << "value: ";
		cin >> value;
	}
}

void CircularLinkedList_print(NODE_t* head)
{
	NODE_t *p = head;
	head = head->next;
	while (p != head)
	{
		cout << head->data << "  ";
		head = head->next;
	}
	cout << head->data;
}

void CircularLinkedList_permutation(NODE_t* head)
{
	NODE_t* p = head;
	while (head->next != p)
	{
		CircularLinkedList_print(head);
		cout << endl;
		head = head->next;
	}
	CircularLinkedList_print(head);
}