#pragma once

typedef int ATOM;
struct NODE_t
{
	ATOM data;
	NODE_t* next;
};

void CircularLinkedList_create(NODE_t*&);
void CircularLinkedList_print(NODE_t*);
void CircularLinkedList_permutation(NODE_t* head);