#include <iostream>
#include "CircularLinkedList.h"
using namespace std;

int main(void)
{
	NODE_t* p = nullptr;
	CircularLinkedList_create(p);
	CircularLinkedList_print(p);
	cout << endl;
	CircularLinkedList_permutation(p);
	return 0;
}