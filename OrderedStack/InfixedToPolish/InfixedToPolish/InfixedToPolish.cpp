#include <iostream>
#include <cstdint>
#include "InfixedToPolish.h"

using namespace std;

void OrderedStack_init(OrderedStack& stck)
{
	stck.stackPointer = -1;
}

bool OrderedStack_isEmpty(OrderedStack stck)
{
	return (-1 == stck.stackPointer);
}

bool OrderedStack_isFull(OrderedStack stck)
{
	return(DIMMAX - 1 == stck.stackPointer);
}

bool OrderedStack_push(OrderedStack& stck, ATOM val)
{
	if (OrderedStack_isFull(stck))
	{
		cout << "Full stack";
		return false;
	}
	else
	{
		stck.data[stck.stackPointer + 1] = val;
		stck.stackPointer++;
		return true;
	}
}

ATOM OrderedStack_pop(OrderedStack& stck)
{
	if (OrderedStack_isEmpty(stck))
	{
		cout << "Empty stack";
		return -1;
	}
	else
	{
		ATOM a = stck.data[stck.stackPointer];
		stck.stackPointer--;
		return a;
	}
}

void OderedStack_print(OrderedStack stck)
{
	for (int i = stck.stackPointer; i >= 0; --i)
	{
		cout << stck.data[i] << "  ";
	}
}

void ConvertToPolish(char* string)
{
	//string is a infixed one
	OrderedStack stck;
	OrderedStack_init(stck);
	char finalString[100];
	int i = 0;
	int j = 0;
	while (i < strlen(string))
	{
		if (string[i] >= '0' && string[i] <= '9')
		{
			finalString[j++] = string[i];
		}
		else
		{
			if (string[i] == '*' || string[i] == '+')	//it's an operator, pop 
			{
				/*((there is an operator at the top of the operator stack with
			greater precedence) or (the operator at the top of the operator stack has
                        equal precedence and
                        the operator is left associative)) and
                      (the operator at the top of the stack is not a left bracket):
					  */
				while ((stck.data[stck.stackPointer]=='*' ||
					stck.data[stck.stackPointer]=='+') && stck.data
					[stck.stackPointer] != '(')
				{
					finalString[j++] = OrderedStack_pop(stck);
				}
				OrderedStack_push(stck, string[i]);
			}
			if (string[i] == '(')
			{
				OrderedStack_push(stck, string[i]);
			}
			if (string[i] == ')')
			{
				while (stck.data[stck.stackPointer] != '(')
				{
					finalString[j++] = OrderedStack_pop(stck);
				}
				OrderedStack_pop(stck);	//pop the '('
			}
		}
		++i;
	}
	while (stck.stackPointer >= 0) //pop the elements that are not in a pair of brackets
	{
		finalString[j++] = OrderedStack_pop(stck);
	}
	finalString[j] = '\0';
	cout << "Final string: " << finalString;
}