#pragma once

#define DIMMAX 100
typedef char ATOM;

struct OrderedStack
{
	ATOM data[DIMMAX];
	int stackPointer;
};

void OrderedStack_init(OrderedStack& stck);
bool OrderedStack_isEmpty(OrderedStack stck);
bool OrderedStack_isFull(OrderedStack stck);
bool OrderedStack_push(OrderedStack& stck, ATOM val);
ATOM OrderedStack_pop(OrderedStack& stck);
void OderedStack_print(OrderedStack stck);

void ConvertToPolish(char* string);