#pragma once

typedef int ATOM;
#define DIMMAX 100

struct OrderedStack
{
	ATOM data[DIMMAX];
	int stackPointer;
};

void OrderedStack_init(OrderedStack&);
bool OrderedStack_isEmpty(OrderedStack);
bool OrderedStack_push(OrderedStack&, ATOM);
ATOM OrderedStack_top(OrderedStack);
ATOM OrderedStack_pop(OrderedStack&);
void OrderedStack_print(OrderedStack);