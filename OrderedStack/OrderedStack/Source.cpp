#include <iostream>
#include "OrderedStack.h"

using namespace std;

int main(void)
{
	OrderedStack a;
	OrderedStack_init(a);
	int x;
	cout << "value: ";
	cin >> x;
	while (x)
	{
		OrderedStack_push(a, x);
		cout << "value: ";
		cin >> x;
	}
	OrderedStack_print(a);
	return 0;
}