#include <iostream>
#include "OrderedStack.h"

using namespace std;

void OrderedStack_init(OrderedStack& stiv)
{
	stiv.stackPointer = -1;
}

bool OrderedStack_isEmpty(OrderedStack stiv)
{
	return(stiv.stackPointer == -1);
}

bool OrderedStack_push(OrderedStack& stiv, ATOM value)
{
	if (stiv.stackPointer <= DIMMAX - 1)
	{
		//nu am stiva plina
		stiv.stackPointer++;
		stiv.data[stiv.stackPointer] = value;
		return true;
	}
	else
	{
		return false;
	}
}

ATOM OrderedStack_top(OrderedStack stiv)
{
	if (OrderedStack_isEmpty(stiv))
	{
		cout << "Empty stack";
		exit(0);
	}
	else
	{
		return stiv.data[stiv.stackPointer];
	}
}

ATOM OrderedStack_pop(OrderedStack& stiv)
{
	if (OrderedStack_isEmpty(stiv))
	{
		cout << "Empty stack";
		exit(0);
	}
	else
	{
		ATOM aux = stiv.data[stiv.stackPointer];
		stiv.stackPointer--;
		return aux;
	}
}

void OrderedStack_print(OrderedStack stiv)
{
	for (int i = stiv.stackPointer; i >= 0; --i)
	{
		cout << stiv.data[i] << "  ";
	}
}