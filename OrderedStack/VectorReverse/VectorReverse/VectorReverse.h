#pragma once

typedef int ATOM;
#define DIMMAX 100

struct OrderedStack
{
	int stackPointer;
	ATOM data[DIMMAX];
};

void OrderedStack_init(OrderedStack& stck);
bool OrderedStack_push(OrderedStack& stck, ATOM val);
bool OrderedStack_isEmpty(OrderedStack stck);
bool OrderedStack_isFull(OrderedStack stck);
ATOM OrderdStack_pop(OrderedStack& stck);
void OrderedStack_print(OrderedStack stck);

void Vector_read(ATOM vct[100], int n);
void Vector_print(ATOM vct[100], int n);
void VectorReverse(ATOM vct[100], int n, OrderedStack& lst);