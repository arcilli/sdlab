#include <iostream>
#include "VectorReverse.h"

using namespace std;

int main(void)
{
	
	OrderedStack stck;
	OrderedStack_init(stck);
	/*
	int x;
	cout << "value: ";
	cin >> x;
	while (x)
	{
		OrderedStack_push(stck, x);
		cout << "value: ";
		cin >> x;
	}
	OrderedStack_print(stck);
	*/
	ATOM vector[100];
	int n;
	cout << "No. elements: ";
	cin >> n;
	Vector_read(vector, n);
	cout << endl<<"Initial array: ";
	Vector_print(vector, n);
	cout << endl << "After reversing: ";

	VectorReverse(vector, n, stck);
	Vector_print(vector, n);
	return 0;
}