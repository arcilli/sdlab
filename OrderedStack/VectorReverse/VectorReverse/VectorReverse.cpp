#include <iostream>
#include "VectorReverse.h"

using namespace std;

void OrderedStack_init(OrderedStack& stck)
{
	stck.stackPointer = -1;
}

bool OrderedStack_push(OrderedStack& stck, ATOM val)
{
	if (OrderedStack_isFull(stck))
	{
		cout << "Full stack";
		return false;
	}
	else
	{
		stck.data[stck.stackPointer + 1] = val;
		stck.stackPointer++;
		return true;
	}
}

bool OrderedStack_isEmpty(OrderedStack stck)
{
	return (-1 == stck.stackPointer);
}

bool OrderedStack_isFull(OrderedStack stck)
{
	return (DIMMAX - 1 == stck.stackPointer);
}

ATOM OrderdStack_pop(OrderedStack& stck)
{
	if (OrderedStack_isEmpty(stck))
	{
		cout << "Empty stack";
	}
	else
	{
		ATOM val = stck.data[stck.stackPointer];
		stck.stackPointer--;
		return val;
	}
}

void OrderedStack_print(OrderedStack stck)
{
	for (int i = stck.stackPointer; i >= 0; --i)
	{
		cout << stck.data[i] << "  ";
	}
}

void Vector_read(ATOM vct[100], int n)
{
	for (int i = 0; i < n; ++i)
	{
		cout << "value: ";
		cin >> vct[i];
	}
}

void Vector_print(ATOM vct[100], int n)
{
	for (int i = 0; i < n; ++i)
	{
		cout << vct[i] << "  ";
	}
}

void VectorReverse(ATOM vct[100], int n, OrderedStack& lst)
{
	for (int i = 0; i < n; ++i)
	{
		OrderedStack_push(lst, vct[i]);
	}
	for (int i = 0; i < n; ++i)
	{
		vct[i] = OrderdStack_pop(lst);
	}
}