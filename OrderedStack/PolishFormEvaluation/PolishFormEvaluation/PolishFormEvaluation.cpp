#include <iostream>
#include <cstdint>
#include "PolishFormEvaluation.h"

using namespace std;

void OrderedStack_init(OrderedStack& stck)
{
	stck.stackPointer = -1;
}

bool OrderedStack_isEmpty(OrderedStack stck)
{
	return (-1 == stck.stackPointer);
}

bool OrderedStack_isFull(OrderedStack stck)
{
	return(DIMMAX - 1 == stck.stackPointer);
}

bool OrderedStack_push(OrderedStack& stck, ATOM val)
{
	if (OrderedStack_isFull(stck))
	{
		cout << "Full stack";
		return false;
	}
	else
	{
		stck.data[stck.stackPointer+1] = val;
		stck.stackPointer++;
		return true;
	}
}

ATOM OrderedStack_pop(OrderedStack& stck)
{
	if (OrderedStack_isEmpty(stck))
	{
		cout << "Empty stack";
		return -1;
	}
	else
	{
		ATOM a = stck.data[stck.stackPointer];
		stck.stackPointer--;
		return a;
	}
}

void OderedStack_print(OrderedStack stck)
{
	for (int i = stck.stackPointer; i >= 0; --i)
	{
		cout << stck.data[i]<<"  ";
	}
}

int Evaluation(char *string)
{
	OrderedStack stck;
	OrderedStack_init(stck);
	int i = 0;
	while (i < strlen(string))
	{
		if (string[i] >= '0' && string[i] <= '9')
		{
			//be careful to use that -'0' properly
			OrderedStack_push(stck, string[i]-'0');
		}
		else
		{
			if (string[i] == '+' || string[i] == '*')
			{
				int t1, t2;
				t1 = OrderedStack_pop(stck);
				t2 = OrderedStack_pop(stck);
				switch (string[i])
				{
				case '+':
					t1 += t2;
					OrderedStack_push(stck, t1);
					break;
				case '*':
					t1 *= t2;
					OrderedStack_push(stck, t1);
					break;
				default:
					cout << "Something wrong just happened";
					break;
				}

			}
		}
		++i;
	}
	if (OrderedStack_isEmpty(stck))
	{
		cout <<endl<< "This should not be empty";
		exit(0);
	}
	ATOM aux = OrderedStack_pop(stck);
	if (!OrderedStack_isEmpty(stck))
	{
		cout << endl << "This should be empty now: "<< "One or more elements remained in the stack";
		exit(0);
	}
	return aux;
}