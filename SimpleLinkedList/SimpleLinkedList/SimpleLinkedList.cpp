#include <iostream>
#include "SimpleLinkedList.h"

using namespace std;

bool SimpleLinkedList_isEmpty(const NODE_t* head)
{
	return (nullptr == head);
}

int SimpleLinkedList_count(const NODE_t* head)
{
	int i = 0;
	while (nullptr != head)
	{
		head = head->next;
		i++;
	}
	return i;
}

bool SimpleLinkedList_insert(NODE_t* &head, ATOM data)
{
	NODE_t* p = new NODE_t;
	p->data = data;
	p->next = head;
	head = p;
	return true;
}

void SimpleLinkedList_print(const NODE_t* head)
{
	cout << endl << "Printing the list: ";
	if (nullptr == head)
	{
		cout << endl << "Empty list.";
	}
	while (nullptr != head)
	{
		cout << head->data <<"  ";
		head = head->next;
	}
	cout << endl;
}

void SimpleLinkedList_sort(NODE_t* head)
{
	while (nullptr != head)
	{
		NODE_t *p = head->next;
		while (nullptr != p)
		{
			if (head->data > p->data)
			{
				ATOM aux;
				aux = head->data;
				head->data = p->data;
				p->data = aux;
			}
			p = p->next;
		}
		head = head->next;
	}
}

bool SimpleLinkedList_search(const NODE_t* head, ATOM aux)
{
	while (nullptr != head)
	{
		if (aux == head->data)
		{
			return true;
			break;
		}
		else
		{
			head = head->next;
		}
	}
	return false;
}

bool SimpleLinkedList_insertOnPosition(NODE_t* &head, int poz, ATOM value)
{
	NODE_t *q = head;
	if (1 == poz)
	{
		SimpleLinkedList_insert(head, value);
		return true;
	}
	while (nullptr != head->next && poz >2)		//poz >2 because it stops before the inserted position
	{
		head = head->next;
		poz--;

	}
	NODE_t *p = new NODE_t;
	p->data = value;
	p->next = head->next;
	head->next = p;
	head = q;
	return true;
}

bool SimpleLinkedList_deleteOnPosition(NODE_t* &head, int poz)
{
	NODE_t *q = head;
	if (1 == poz)
	{
		SimpleLinkedList_deleteHead(head);
		return true;
	}

	//merg pana la 2 ca sa ma opresc inaintea nodului pe care eu vreau sa-l sterg,
	//altfel n-am cum sa refac legaturile
	while (poz > 2)
	{
		head = head->next;
		poz--;
	}
	if (nullptr != head)
	{
		NODE_t *p;
		p = head->next;
		head->next= (head->next)->next;
		delete p;
		p = nullptr;
		head = q;
		return true;
	}
	return false;
}

void SimpleLinkedList_deleteHead(NODE_t* &head)
{
	if (head != nullptr)
	{
		NODE_t * p = head;
		head = p->next;
		delete p;
		p = nullptr;
	}
}

//merge 2 lists
void SimpleLinkedList_merge(NODE_t* head, NODE_t * const head2)
{
	while (nullptr != head->next)
	{
		head = head->next;
	}
	head->next = head2;
}

void SimpleLinkedList_mergeFromBoth(NODE_t* head, NODE_t* head2, NODE_t *aux)
{
	//WORK for implementation here
}

void SimpleLinkedList_reverseLink(NODE_t* &head)
{
	NODE_t *p, *q, *r;
	p = head;
	q = p->next;
	r = q->next;
	p->next = nullptr;
	q->next = p;
	while (nullptr != r->next)
	{
		p = q;
		q = r;
		r = r->next;
		q->next = p;
	}
	r->next = q;
	head = r;
}

void SimpleLinkedList_delete(NODE_t* &head)
{
	while (nullptr != head)
	{
		SimpleLinkedList_deleteHead(head);
	}
}

bool SimpleLinkedList_isCircular(NODE_t* head)
{
	NODE_t* p = head;
	while (nullptr != head)
	{
		head = head->next;
		if (head == p)
		{
			return true;
		}
	}
	return false;
}

int SimpleLinkedList_indexOf(NODE_t* head, ATOM data)
{
	int i = 1, ok = 0;
	while (nullptr != head && !ok)
	{
		if (head->data == data)
		{
			ok = 1;
			return i;
		}
		else
		{
			++i;
			head = head->next;
		}
	}
	return 0;
}

NODE_t* SimpleLinkedList_createNode(ATOM value)
{
	if (value)
	{
		NODE_t *p = new NODE_t;
		p->data = value;
		cout << "value: ";
		cin >> value;
		p->next = SimpleLinkedList_createNode(value);
		return p;
	}
	else
		//ultimul caz, trebuie sa pun utimul nod ->next pe nullptr
	{
		return nullptr;
	}
}

int SimpleLinkedList_findMinimumValue(const NODE_t* head)
{
	int val = head->data;
	int i = 1, poz = 1;
	while (nullptr != head)
	{
		if (val > head->data)
		{
			val = head->data;
			poz = i;
		}
		head = head->next;
		++i;
	}
	return poz;
}