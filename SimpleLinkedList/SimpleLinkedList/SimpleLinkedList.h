#pragma once
typedef int ATOM;

struct NODE_t
{
	ATOM data;
	NODE_t* next;
};

bool SimpleLinkedList_isEmpty(const NODE_t* head);
int SimpleLinkedList_count(const NODE_t* head);
bool SimpleLinkedList_insert(NODE_t* &head, ATOM);
void SimpleLinkedList_print(const NODE_t* head);
bool SimpleLinkedList_search(const NODE_t* head, ATOM);
void SimpleLinkedList_sort(NODE_t* head);
bool SimpleLinkedList_insertOnPosition(NODE_t * &head, int poz, ATOM value);

//delete head function
void SimpleLinkedList_deleteHead(NODE_t* &head);

//delete on position function
bool SimpleLinkedList_deleteOnPosition(NODE_t* &head, int poz);

//merge function
void SimpleLinkedList_merge(NODE_t* head, NODE_t * const head2);

//reverseLink
void SimpleLinkedList_reverseLink(NODE_t* &head);

//interclasare
//NOT IMPLEMENTED YET
void SimpleLinkedList_mergeFromBoth(NODE_t* head, NODE_t* head2);

//delete list
void SimpleLinkedList_delete(NODE_t* &head);

//check if list its circular
bool SimpleLinkedList_isCircular(NODE_t* head);

//return index of @data if its found
int SimpleLinkedList_indexOf(NODE_t* head, ATOM data);

//recursive function for creating a SimpleLinkedList
NODE_t* SimpleLinkedList_createNode(ATOM);

//find the minimum/maximum value & return the index
int SimpleLinkedList_findMinimumValue(const NODE_t* head);
