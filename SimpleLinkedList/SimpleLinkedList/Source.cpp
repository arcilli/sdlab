#include <iostream>
#include "SimpleLinkedList.h"

using namespace std;

int main(void)
{
	int x;
	NODE_t* head=nullptr, *head2=nullptr;
	cout << "value: ";
	cin >> x;
	while (x)
	{
		SimpleLinkedList_insert(head, x);
		cout << "value: ";
		cin >> x;
	}
	SimpleLinkedList_print(head);
	/*
	SimpleLinkedList_isEmpty(head) ? cout << "Empty " : cout << "Not empty";
	cout << endl << "Lets search something: ";
	cin >> x;
	(SimpleLinkedList_search(head, x)) ? cout << "Found it" : cout << "Not found";
	cout << endl << "Sorting the list: ";
	SimpleLinkedList_sort(head);
	SimpleLinkedList_print(head);
	cout << endl << "Lets insert something on a position: ";
	cout << endl << "Value, position: ";
	int pos;
	cin >> x >> pos;
	SimpleLinkedList_insertOnPosition(head, pos, x);
	SimpleLinkedList_print(head);
	cout << endl << "Delete something on position: ";
	cin >> x;
	SimpleLinkedList_deleteOnPosition(head, x);
	SimpleLinkedList_print(head);
	cout << endl << "Testing merge function"<<endl;
	cout << "value: ";
	cin >> x;
	while (x)
	{
		SimpleLinkedList_insert(head2, x);
		cout << "value: ";
		cin >> x;
	}
	SimpleLinkedList_print(head2);
	SimpleLinkedList_merge(head, head2);
	SimpleLinkedList_print(head);
	SimpleLinkedList_reverseLink(head);
	SimpleLinkedList_print(head);

	//testing if it's circular
	NODE_t *p = head;
	while (nullptr != head->next)
	{
		head = head->next;
	}
	head->next = p;
	cout << SimpleLinkedList_isCircular(head);
	cout << SimpleLinkedList_count(head);
	cout << "Return the index of: ";
	cin >> x;
	cout << SimpleLinkedList_indexOf(head, x);
	NODE_t* p;
	p = SimpleLinkedList_createNode(1);
	SimpleLinkedList_print(p);
	*/
	cout << SimpleLinkedList_findMinimumValue(head);
	return 0;
}